<?php

/**
 * Template Name: Map
 */
?>

<?php get_header(); ?>

<main id="primary" class="site-main">
    <div id="story" class="site-main">
        <!-- Front page -->
        <section style="background-image: url('<?php echo get_theme_mod('map_bg'); ?>')">
            <h1 class="page-title">
                Where can I find <?php echo get_bloginfo(); ?> ?
            </h1>
            <p class="p-desc map-sentence">Find all the rooms where you can play <?php echo get_bloginfo() ?>. </p>
            <?php
            get_template_part('template-parts/content', 'map');
            ?>
        </section>
    </div>
</main><!-- #main -->


<?php
get_footer();
