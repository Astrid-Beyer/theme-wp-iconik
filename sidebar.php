<?php

/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package iconik_jv
 */

?>
	<p class="site-description bold">
		<?php
		$iconik_jv_description = get_bloginfo('description', 'display');
		echo $iconik_jv_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped 
		?>
	</p>