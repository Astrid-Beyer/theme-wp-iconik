<?php

/**
 * Template Name: Assets
 */
?>

<?php get_header(); ?>

<main id="primary" class="site-main">
    <div id="story" class="site-main">
        <section style="background-image: url('<?php echo get_theme_mod('assets_bg'); ?>')">
            <div class="heading-article">
                <h1>Assets</h1>
            </div>
            
            <?php get_template_part('template-parts/content', 'page'); ?>
        </section>
    </div>
</main><!-- #main -->


<?php
get_footer();
