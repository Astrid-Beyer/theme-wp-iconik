<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package iconik_jv
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php iconik_jv_post_thumbnail(); ?>

	<div class="map-content">
		<?php
		the_content();

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'iconik_jv' ),
				'after'  => '</div>',
			)
		);
		?>
	</div><!-- .map-content -->
</article><!-- #post-<?php the_ID(); ?> -->
