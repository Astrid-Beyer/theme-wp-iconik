<?php

/**
 * Template Name: Aide (support)
 */
?>

<?php get_header(); ?>

<main id="primary" class="site-main">
    <div id="story" class="site-main">
        <section style="background-image: url('<?php echo get_theme_mod('support_bg'); ?>')">
            <?php get_template_part('template-parts/content', 'page'); ?>
        </section>
    </div>
</main><!-- #main -->


<?php
get_footer();
