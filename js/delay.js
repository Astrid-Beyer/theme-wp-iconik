/**
 * File delay.js.
 *
 * switch between article from Iconik & from Steam
 * switch between link for responsive & desktop on the PLAY button
 */

(function ($) {
  $("#switch-actu").on("click", function () {
    if ($("#switch-actu").text() == "See Steam's article")
      $("#switch-actu").text($("#switch-actu").text().replace("Steam", "website"));
    else $("#switch-actu").text($("#switch-actu").text().replace("website", "Steam"));


    if ($("#switch-actu").text() == "Voir les articles sur Steam")
      $("#switch-actu").text($("#switch-actu").text().replace("Steam", "le site"));
    else $("#switch-actu").text($("#switch-actu").text().replace("le site", "Steam"));

    $("#steam-articles").toggle();
    $("#website-articles").toggle();
  });

  if (window.matchMedia("only screen and (max-width: 760px)").matches && scriptPlay.resp)
    $(".play").attr("href", scriptPlay.resp);
  else if (!(window.matchMedia("only screen and (max-width: 760px)").matches) && scriptPlay.desktop)
    $(".play").attr("href", scriptPlay.desktop);

}(jQuery));