# Thème WordPress ICONIK_JV

Un thème conçu pour les sites de jeu d'Iconik.

## Comment installer ce thème ?
1. Se rendre sur son tableau de bord WP, Apparence > Thèmes et cliquer sur "Ajouter un thème".
2. Cliquer sur "Téléverser un thème" et choisir Fichier, selectionner le fichier .zip du thème. Cliquer sur Installer maintenant.
3. Cliquer sur Activer pour utiliser le nouveau thème.

## Infos complémentaires
Ajouter tout d'abord un **Menu Primary** sur l'interface Wordpress (bouton "Personnaliser" quand on est connecté en tant qu'administrateur) avec toutes les pages nécessaires pour que la navbar comporte les éléments souhaités. Dans ce menu **Primary**, ajouter un bouton Opposite Language (Language Switcher) depuis le plug-in **TranslatePress**. Le thème va détecter ce bouton dans la barre de navigation, le rendre invisible et simulera un clic dessus lorsqu'un utilisateur clique sur le drapeau en haut à droite du thème. Sans ça, le bouton de drapeau en haut à droite du thème est inutile. Attention à bien définir la langue par défaut sur Anglais.

Il est nécessaire de créer un **Menu** "Footer" pour le bas de page également afin d'y glisser les mentions légales, politique de confidentialité...

On a alors :

**Menu Primary** :
> Home

> Assets

> *Where to play ?*

> News

> Help

> Opposite Language

**Menu Footer** :
> Terms and Conditions

> Privacy Policy

> Cookies Policy

> Contact

/!\ Construire la page principale avec le plug-in `WPBakery Page Builder`. Les pages possédants une customisation dans le menu Personnaliser n'ont pas besoin de ce plug-in ! Le thème **associé au plug-in** devrait rendre le site **responsive** sur tablette et smartphone. Certains éléments sur la page principale doivent être ajustés avec le plug-in de construction de page, WPBakery. Voir les sites Line Of Fire et King Pong pour un exemple de construction. 

La page d'accueil doit être une **page statique** (voir panneau d'administration WP, Réglages, Lecture, cocher "La page d'accueil affiche **une page statique**) dotée de la template "Page d'Accueil". Nommer cette page avec le titre du jeu.

Pour mettre une template sur une page :
> Modifier la page;

> Dans l'onglet page, se rendre sur le menu **Modèle**

> Sélectionner dans le menu déroulant la template associée à la page

Bien décrire pour chaque page sa **template** (pour la page map : donner la template Map, page principale : donner la template Page d'Accueil...)

Customiser les champs du thème dans le bouton Personnaliser (ou Customization en Anglais) :

* **Détails du thème** : 

• Sélecteur de couleur pour changer l'ambiance du thème. Peut être utilisé sur des titres, ou des éléments à mettre en avant. S'applique sur tous les éléments comportant la classe `.highlights`. Cette classe est applicable sur des éléments grâce au plug-in `WPBakery Page Builder`.

• Possibilité de changer la source du lien du bouton "PLAY" sur le site. De base, ce bouton redirige vers une ancre #play sur la page principale du site. Cette ancre est renseignable via `WPBakery Page Builder`, dans le champs "ID de l'élément". On peut alors, avec cette option, modifier la cible du lien lorsqu'on est sur smartphone.

* **Page Asset** : Permet de changer le fond de section de la page Asset. Prévoir une image assez longue en hauteur.

* **Page Où Jouer** : Permet de changer le fond de section de la page Où Jouer. Cette page n'existe que pour les sites existants en salle d'arcade VR. Prévoir une image assez longue en hauteur.

* **Page Actualités** : Permet de changer le fond de section des actualités. Permet également de modifier les flux RSS qui apparaissent sur la page. /!\ Par défaut, le flux RSS Steam est rempli : bien penser à le modifier. Le champ pour Steam peut être laissé vide, la template s'adaptera et retirera la mention "Voir les articles de Steam". Possibilité de renseigner un flux RSS anglophone en plus de celui du blog francophone.

* **Page Aide** : Permet de changer le fond de section de la page de support.

* **Réseaux footer** : tant que ce champ de customisation n'est pas rempli, aucun réseau social ne sera visible dans le footer.


Si besoin, dans le fichier `.htaccess` (fichier root du Wordpress), ajouter les lignes suivantes afin de pouvoir publier des images de grande taille :

```
php_value upload_max_filesize 64M
php_value post_max_size 128M
php_value memory_limit 256M
php_value max_execution_time 300
php_value max_input_time 300
```

## Passer le site en HTTPS

Se rendre d'abord sur le back de WordPress, Réglages > Général, et dans les champs "Adresse web de WordPress (URL)" et "Adresse web du site (URL)", remplacer "http" par "https".

Ensuite, modifier le fichier .htacess situé à la racine de chacun des sites pour écrire :

```
# Begin Force HTTPS
RewriteEngine On
RewriteCond %{HTTPS} !=on
RewriteRule ^/?(.*) https://URL_DU_SITE/$1 [R,L]
# End Force HTTPS
```

Où URL_DU_SITE peut être "studio.iconik.com" par exemple.

Faire CTRL+F5 pour nettoyer le cache navigateur en rafraichissant la page pour vérifier que le site est bien passé en HTTPS.

# Crédits
* Based on Underscores https://underscores.me/, (C) 2012-2020 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2018 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)

Thème réalisé avec ♥ pour Iconik© par Astrid BEYER, 2022