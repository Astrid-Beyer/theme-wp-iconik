<?php

/**
 * iconik_jv functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package iconik_jv
 */

if (!defined('ICONIK_JV_VERSION')) {
	// Replace the version number of the theme on each release.
	define('ICONIK_JV_VERSION', '1.0.0');
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function iconik_jv_setup()
{
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on iconik_jv, use a find and replace
		* to change 'iconik_jv' to the name of your theme in all the template files.
		*/
	load_theme_textdomain('iconik_jv', get_template_directory() . '/languages');

	// Add default posts and comments RSS feed links to head.
	add_theme_support('automatic-feed-links');

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support('title-tag');

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support('post-thumbnails');

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'menu-1' => esc_html__('Primary', 'iconik_jv'),
		)
	);

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support('customize-selective-refresh-widgets');

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */
	add_theme_support(
		'custom-logo',
		array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);
}
add_action('after_setup_theme', 'iconik_jv_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function iconik_jv_content_width()
{
	$GLOBALS['content_width'] = apply_filters('iconik_jv_content_width', 640);
}
add_action('after_setup_theme', 'iconik_jv_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function iconik_jv_widgets_init()
{
	register_sidebar(
		array(
			'id'            => 'histoire',
			'name'          => esc_html__('Histoire', 'iconik_jv'),
			'description'   => esc_html__('Add widgets in main content.', 'iconik_jv'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
		)
	);
	/*
	register_sidebar(
		array(
			'id'            => 'infos',
			'name'          => esc_html__('Infos', 'iconik_jv'),
			'description'   => esc_html__('Surface (free roaming - 11 à 20m², Box - 2 à 3m²), nombre de joueurs, durée en minutes', 'iconik_jv'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
	);
*/
	register_sidebar(
		array(
			'id'            => 'key_features',
			'name'          => esc_html__('Key-Features', 'iconik_jv'),
			'description'   => esc_html__('Add widgets in keyfeatures content.', 'iconik_jv'),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
	);
}
add_action('widgets_init', 'iconik_jv_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function iconik_jv_scripts()
{
	wp_enqueue_style('iconik_jv-style', get_stylesheet_uri(), array(), ICONIK_JV_VERSION);
	wp_style_add_data('iconik_jv-style', 'rtl', 'replace');

	wp_enqueue_script('navbar-js',  get_template_directory_uri() . '/js/navbar.js', array('jquery'), '1.0.0');
	wp_enqueue_script('delay-js',  get_template_directory_uri() . '/js/delay.js', array('jquery'), '1.0.0', true);
	wp_localize_script('delay-js', 'scriptPlay', array('resp' => get_theme_mod('link_resp'), 'desktop' => get_theme_mod('link_desktop')));

	wp_enqueue_script('iconik_jv-navigation', get_template_directory_uri() . '/js/navigation.js', array(), ICONIK_JV_VERSION, true);

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}
add_action('wp_enqueue_scripts', 'iconik_jv_scripts');


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
	require get_template_directory() . '/inc/jetpack.php';
}

function mytheme_customize_css()
{
	$colorPicker = get_theme_mod('color_picker');
	if (!empty($colorPicker)) :
?>
		<style type="text/css">
			.highlights {
				color: <?php echo $colorPicker; ?>;
			}
		</style>
<?php
	endif;
}
add_action('wp_head', 'mytheme_customize_css');


/**
 * Customization!! 
 * 
 * All our sections, settings, and controls will be added here
 * https://developer.wordpress.org/themes/customize-api/customizer-objects/
 * 
 */

function customize_register($wp_customize)
{
	/**
	 * IMAGE EMBED
	 */

	$wp_customize->add_setting('embed', array(
		'sanitize_callback' => 'esc_url_raw',
	)); // setting

	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'embed_control', array(
		'section' => 'title_tagline',
		'settings' => 'embed',
		'label' => __('Remplacer l\'image du site quand on partage son URL'),
		'button_labels' => array( // All These labels are optional
			'select' => 'Select img',
			'remove' => 'Remove img',
			'change' => 'Change img',
		)
	))); // control

	/*** Actualités ***/
	/* background */
	$wp_customize->add_section('actu_section', array(
		'title'      => __('Page Actualités', 'iconik_jv'),
		'description' => 'Modifier la page Actualités. Si il n\'y a pas de page d\'actualités, créer une nouvelle page avec la template "Actualités".',
		'priority'   => 25,
	));

	$wp_customize->add_setting('actu_bg', array(
		'sanitize_callback' => 'esc_url_raw',
	)); // setting

	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'actu_bg_control', array(
		'section' => 'actu_section',
		'settings' => 'actu_bg',
		'label' => __('Modifier image de fond'),
		'button_labels' => array( // All These labels are optional
			'select' => 'Select img',
			'remove' => 'Remove img',
			'change' => 'Change img',
		)
	))); // control


	/**** RSS *****/
	/* site */
	$wp_customize->add_setting('rss-website', array(
		'transport' => 'refresh',
	)); // setting
	$wp_customize->add_control('rss-website-control', array(
		'label'      => __('Flux RSS', 'iconik_jv'),
		'description' => ('Flux RSS Iconik contenant les articles de la catégorie du jeu'),
		'section'    => 'actu_section',
		'settings'   => 'rss-website',
	)); // control

	/* site anglais*/
	$wp_customize->add_setting('rss-website-eng', array(
		'transport' => 'refresh',
	)); // setting
	$wp_customize->add_control('rss-website-eng-control', array(
		'description' => ('Flux RSS <b>anglophone</b> d\'Iconik contenant les articles de la catégorie du jeu'),
		'section'    => 'actu_section',
		'settings'   => 'rss-website-eng',
	)); // control

	/* steam */
	$wp_customize->add_setting('rss-steam', array(
		'transport' => 'refresh',
	)); // setting
	$wp_customize->add_control('rss-steam-control', array(
		'description' => ('Flux RSS Steam du jeu, s\'il existe sur Steam. Laisser vide sinon'),
		'section'    => 'actu_section',
		'settings'   => 'rss-steam',
	)); // control

	/**** API YouTube ****/
	$wp_customize->add_setting('api-yt', array(
		'default'   => '',
		'transport' => 'refresh',
	)); // setting
	$wp_customize->add_control('api-yt-control', array(
		'label'      => __('API YouTube', 'iconik_jv'),
		'description' => ('Donner une clé API. Permet d\'afficher les vidéos d\'une playlist dans les actualités.'),
		'section'    => 'actu_section',
		'settings'   => 'api-yt',
	)); // control

	$wp_customize->add_setting('playlist-yt', array(
		'default'   => '',
		'transport' => 'refresh',
	)); // setting
	$wp_customize->add_control('playlist-yt-control', array(
		'description' => ('Lien de la playlist associée au jeu'),
		'section'    => 'actu_section',
		'settings'   => 'playlist-yt',
	)); // control


	/*** Assets ***/
	/* background */
	$wp_customize->add_section('assets_section', array(
		'title'      => __('Page Asset', 'iconik_jv'),
		'description' => 'Modifier la page Asset. Si il n\'a y pas de page d\'assets, créer une nouvelle page avec la template "Asset".',
		'priority'   => 23,
	));

	$wp_customize->add_setting('assets_bg', array(
		'sanitize_callback' => 'esc_url_raw',
	)); // setting

	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'assets_bg_control', array(
		'section' => 'assets_section',
		'settings' => 'assets_bg',
		'label' => __('Modifier image de fond'),
		'button_labels' => array( // All These labels are optional
			'select' => 'Select img',
			'remove' => 'Remove img',
			'change' => 'Change img',
		)
	))); // control


	/*** Où jouer ***/
	/* background */
	$wp_customize->add_section('map_section', array(
		'title'      => __('Page Où Jouer', 'iconik_jv'),
		'description' => 'Modifier la page Où Jouer. Si cette page n\'existe pas, créer une nouvelle page avec la template "Map".',
		'priority'   => 24,
	));

	$wp_customize->add_setting('map_bg', array(
		'sanitize_callback' => 'esc_url_raw',
	)); // setting

	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'map_bg_control', array(
		'section' => 'map_section',
		'settings' => 'map_bg',
		'label' => __('Modifier image de fond'),
		'button_labels' => array( // All These labels are optional
			'select' => 'Select img',
			'remove' => 'Remove img',
			'change' => 'Change img',
		)
	))); // control


	/*** Support ***/
	/* background */
	$wp_customize->add_section('support_section', array(
		'title'      => __('Page Aide', 'iconik_jv'),
		'description' => 'Modifier la page Aide. Si il n\'a y pas de page d\'aide, créer une nouvelle page avec la template "Aide (support)".',
		'priority'   => 26,
	));

	$wp_customize->add_setting('support_bg', array(
		'sanitize_callback' => 'esc_url_raw',
	)); // setting

	$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'support_bg_control', array(
		'section' => 'support_section',
		'settings' => 'support_bg',
		'label' => __('Modifier image de fond'),
		'button_labels' => array( // All These labels are optional
			'select' => 'Select img',
			'remove' => 'Remove img',
			'change' => 'Change img',
		)
	))); // control


	/*** Color hightlight + PLAY link ***/
	$wp_customize->add_section('theme_section', array(
		'title'      => __('Détails du thème', 'iconik_jv'),
		'description' => '',
		'priority'   => 22,
	));

	$wp_customize->add_setting('color_picker', array(
		'default'     => '#000000',
		'transport'   => 'refresh',
	)); // setting

	$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'color_control', array(
		'section' => 'theme_section',
		'settings' => 'color_picker',
		'label' => __('Modifier la couleur des éléments comportant la classe "highlights".'),
	))); // control

	$wp_customize->add_setting('link_resp', array(
		'transport' => 'refresh',
	)); // setting
	$wp_customize->add_control('link_resp_control', array(
		'label'    		=> __('Changer le lien du bouton PLAY selon si on visualise le site sur version mobile ou PC.', 'iconik_jv'),
		'description' 	=> ('Lien pour version smartphone (page Map, lien Steam du jeu...)'),
		'section' 		=> 'theme_section',
		'settings' 		=> 'link_resp',
	)); // control

	$wp_customize->add_setting('link_desktop', array(
		'transport'   => 'refresh',
	)); // setting
	$wp_customize->add_control('link_desktop_control', array(
		'description' => ('Lien pour version bureau (ancre vers la page principale avec "#play" ou autre...)'),
		'section'     => 'theme_section',
		'settings'    => 'link_desktop',
	)); // control


	/*** Réseaux sociaux dans le footer 
	 * ------------------------------------------------***/
	$wp_customize->add_section('footer', array(
		'title'      => __('Réseaux footer', 'iconik_jv'),
		'description' => 'Laisser un champ vide pour retirer un réseau',
		'priority'   => 30,
	));

	// twitter
	$wp_customize->add_setting('footer-twt', array(
		'default'   => '', /* https://twitter.com/IconikHybrid */
		'transport' => 'refresh',
	));
	$wp_customize->add_control('footer_control_twt', array(
		'label'      => __('Lien Twitter', 'iconik_jv'),
		'section'    => 'footer',
		'settings'   => 'footer-twt',
	));

	// instagram
	$wp_customize->add_setting('footer-ig', array(
		'default'   => '', /* https://www.instagram.com/iconik.hybridmedia/ */
		'transport' => 'refresh',
	));
	$wp_customize->add_control('footer_control_ig', array(
		'label'      => __('Lien instagram', 'iconik_jv'),
		'section'    => 'footer',
		'settings'   => 'footer-ig',
	));

	// facebook
	$wp_customize->add_setting('footer-fb', array(
		'default'   => '', /* https://www.facebook.com/iconik.hybridmedia/  */
		'transport' => 'refresh',
	));
	$wp_customize->add_control('footer_control_fb', array(
		'label'      => __('Lien Facebook', 'iconik_jv'),
		'section'    => 'footer',
		'settings'   => 'footer-fb',
	));

	// youtube
	$wp_customize->add_setting('footer-yt', array(
		'default'   => '', /* https://www.youtube.com/iconik */
		'transport' => 'refresh',
	));
	$wp_customize->add_control('footer_control_yt', array(
		'label'      => __('Lien YouTube', 'iconik_jv'),
		'section'    => 'footer',
		'settings'   => 'footer-yt',
	));

	// linkedin
	$wp_customize->add_setting('footer-li', array(
		'default'   => '', /* https://mp.linkedin.com/company/iconikhybridmedia */
		'transport' => 'refresh',
	));
	$wp_customize->add_control('footer_control_li', array(
		'label'      => __('Lien LinkedIn', 'iconik_jv'),
		'section'    => 'footer',
		'settings'   => 'footer-li',
	));

	// discord
	$wp_customize->add_setting('footer-disc', array(
		'default'   => '', /* https://discord.gg/iconik */
		'transport' => 'refresh',
	));
	$wp_customize->add_control('footer_control_disc', array(
		'label'      => __('Lien Discord', 'iconik_jv'),
		'section'    => 'footer',
		'settings'   => 'footer-disc',
	));

	// steam
	$wp_customize->add_setting('footer-steam', array(
		'default'   => '', /* https://store.steampowered.com/developer/ICONIK */
		'transport' => 'refresh',
	));
	$wp_customize->add_control('footer_control_steam', array(
		'label'      => __('Lien Steam', 'iconik_jv'),
		'section'    => 'footer',
		'settings'   => 'footer-steam',
	));
}
add_action('customize_register', 'customize_register');
