<?php

/**
 * Template Name: Actualités
 */
?>

<?php get_header(); ?>

<main id="primary" class="site-main">
    <div id="story" class="site-main">
        <section style="background-image: url('<?php echo get_theme_mod('actu_bg'); ?>')">
            <div class="heading-article">
                <h1>Last articles</h1>
                <?php
                if (get_theme_mod('rss-steam') && get_theme_mod('rss-website')) {
                    $firstLine = '<a href="#" id="switch-actu" class="italic">See Steam\'s article</a>';
                    echo $firstLine;
                }
                ?>
            </div>
            <div class="actu">
                <div class="page-title actu">
                    <div class="actu-article">

                        <div class="all-articles">
                            <?php
                            $rss_steam = get_theme_mod('rss-steam');
                            if ($rss_steam) {
                                echo "<div id='steam-articles' " . (get_theme_mod('rss-website') ? "style='display:none;'" : "") . ">";
                                $rss_load = simplexml_load_file($rss_steam);
                                foreach ($rss_load->channel->item as $item) {
                                    echo '<div class="actu-link-article">
                                        <img src="' .  $item->enclosure['url'] . '" class="actu-logo">
                                        <div class="actu-title-desc">
                                            <h3 class="bold">' . $item->title . '</h3>
                                            <p><a href="' . $item->link . '" class="bold actu-link" target="_blank">See article</a><br />
                                                <span class="actu-date"><span>Published on</span> ' . date("l d M Y", strtotime($item->pubDate)) . '</span>
                                            </p>
                                        </div> <!-- .actu-title-desc -->
                                        </div> <!-- .actu-link-article -->';
                                }
                                echo "</div> <!-- #steam-articles -->";
                            }
                            ?>
                            <div id="website-articles">

                                <?php
                                $return = '';
                                if (get_theme_mod('rss-website')) {
                                    if (get_locale() == 'fr_FR' || !get_theme_mod('rss-website-eng'))
                                        $rss_link = get_theme_mod('rss-website');
                                    else if (get_locale() == 'en_EN' && get_theme_mod('rss-website-eng'))
                                        $rss_link = get_theme_mod('rss-website-eng');
                                    else
                                        $rss_link = get_theme_mod('rss-website-eng');

                                    $rss_load = simplexml_load_file($rss_link);



                                    foreach ($rss_load->channel as $channel) {
                                        $return .= '<div class="actu-link-article">
                                                    <img src="' . $channel->image->url . '" class="actu-logo">
                                                    <div class="actu-title-desc">
                                                        <h3 class="bold">' . $channel->item->title . '</h3>
                                                        <a href="' . $channel->item->link . '" class="bold actu-link" target="_blank">See more</a><br/>
                                                        <span class="actu-date">Published on ' . date("l d M Y", strtotime($channel->item->pubDate)) . '</span></p>
                                                        </div> <!-- .actu-title-desc -->
                                                        </div> <!-- .actu-link-article -->';
                                    }

                                    $return .= "</div> <!-- .website-articles --> 
                                                </div> <!-- all-articles -->
                                                </div> <!-- actu-articles -->
                                                </div> <!-- .page-title-actu -->";
                                } else if (!get_theme_mod('rss-website') && !get_theme_mod('rss-steam')) {
                                    $return .= '<div class="actu-link-article">
                                                <img src="' . get_bloginfo('template_url') . '/img/logo-pink.png" class="actu-logo">
                                                <div class="actu-title-desc">
                                                    <h3 class="bold">Modifier le flux RSS </h3>
                                                    <p>Se rendre sur le menu "Personnaliser" afin de fournir le flux RSS correspondant aux articles du jeu.
                                                    Il est possible de publier également le flux RSS des articles Steam.</p>
                                                </div> <!-- .actu-title-desc -->
                                            </div> <!-- .actu-link-article -->
                                        </div><!-- .website-articles -->
                                        </div> <!-- .all-articles -->
                                        </div><!-- .actu-article -->
                                    </div> <!-- .page-title-actu -->';
                                } else if (!get_theme_mod('rss-website')) {
                                    $return .= "</div><!-- .website-articles -->
                                        </div> <!-- .all-articles -->
                                        </div><!-- .actu-article -->
                                    </div> <!-- .page-title-actu -->";
                                }
                                echo $return;
                                ?>

                                <?php

                                $clientLink = get_theme_mod('playlist-yt');
                                $ytLinkPrefix = "https://www.youtube.com/playlist?list=";

                                if (str_contains($clientLink, $ytLinkPrefix)) {
                                    $clientLink = str_replace($ytLinkPrefix, "", $clientLink);
                                } else if ($clientLink != '') {
                                    echo '<p>Erreur. L\'URL de la playlist devrait ressembler à quelque chose comme 
                                        https://www.youtube.com/playlist?list= ... .</p>';
                                }


                                $option = array(
                                    'playlistId' => $clientLink,
                                    'key' =>  get_theme_mod('api-yt')
                                );
                                if ($option['playlistId'] == null) {
                                    echo '<p>Se rendre sur le menu "Personnaliser" pour ajouter une playlist.</p>';
                                } else {
                                    if ($option['key'] == null) {
                                        echo '<p>Renseigner une clé API de YouTube sur le menu "Personnaliser".</p>';
                                    } else if ($option['key'] == "oui") {
                                    } else {
                                        $url = 'https://youtube.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=' . $option['playlistId'] . '&key=' . $option['key'];

                                        $curl = curl_init($url);
                                        curl_setopt($curl, CURLOPT_CAINFO, __DIR__ . DIRECTORY_SEPARATOR . 'cert.cer');
                                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

                                        $data = curl_exec($curl);
                                        if ($data === false) var_dump(curl_error($curl));
                                        else {
                                            $data = json_decode($data, true);
                                            $ids = array_map(function ($vid) {
                                                return $vid['snippet']['resourceId']['videoId'];
                                            }, $data['items']);
                                            //var_dump($ids); // id des vidéos

                                            $return = '<div class="actu-yt">';
                                            foreach ($ids as $videoId) {
                                                $return .= '<div class="ytbe-playlist">
                                                                <iframe id="player" type="text/html" src="https://www.youtube.com/embed/' . $videoId . '" frameborder="0"></iframe>
                                                            </div>';
                                            }
                                            echo $return;
                                        }
                                    }
                                    curl_close($curl);
                                }
                                ?>
                            </div> <!-- .actu-yt -->
                        </div> <!-- .actu -->

        </section>
    </div>
</main><!-- #main -->


<?php
get_footer();
