<?php

/**
 * Template Name: Page d'accueil
 */
?>

<?php get_header(); ?>

<main id="primary" class="site-main">
    <div id="story" class="site-main">
        <!-- Front page -->
        <?php get_template_part('template-parts/content', 'page'); ?>
    </div>
</main><!-- #main -->


<?php
get_footer();
